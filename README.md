# Api/casoft Init Projets

## Description

Dépôt des supports des projets :
 - Présentation (jour 1)
 - Liste des sujets proposés (jour 3)

**En cas de questions :** [thibaud.duhautbout@hds.utc.fr](mailto://thibaud.duhautbout@hds.utc.fr).

Pour aller plus loin : lire la [documentation de git](https://git-scm.com/docs).

En cas de remarques sur la présentation (ou de questions aussi), vous pouvez utiliser le [système d'issues](https://gitlab.utc.fr/picasoft/apis/h19/init/projets/issues).

## Présentations

Une chaîne d'intégration permet de construire automatiquement le PDF à partir du fichier .tex présent à la racine.

Les chemins d'upload sont définis dans le fichier [.gitlab-ci.yml](./gitlab-ci.yml).

Les présentations au format PDF est disponibles :
 - [Présentation, méthodes agiles](https://uploads.picasoft.net/api/projets_methode.pdf).
 - [Choix des sujets](https://uploads.picasoft.net/api/projets_sujets.pdf).
