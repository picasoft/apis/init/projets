all:
	pdflatex presentation.tex
	pdflatex presentation.tex
	pdflatex projets.tex
	pdflatex projets.tex

presentation:
	pdflatex "\def\Release{PRESENTATION}\input{presentation}"
	pdflatex "\def\Release{PRESENTATION}\input{presentation}"
	pdflatex "\def\Release{PRESENTATION}\input{projets}"
	pdflatex "\def\Release{PRESENTATION}\input{projets}"

clean:
	rm *.aux
	rm *.log
	rm *.nav
	rm *.out
	rm *.snm
	rm *.toc
	rm *.vrb
